FROM python:3-alpine3.8

RUN pip3 install requests

COPY weather_bonus_2.py weather.py

ENTRYPOINT ["python3", "weather.py"]
