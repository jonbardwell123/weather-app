#! /usr/bin/env python3
import requests
import argparse
import datetime

# noinspection SpellCheckingInspection
DARK_SKY_SECRET_KEY = "fbcf15d3d872f2f66720690d60e26343"

parser = argparse.ArgumentParser(description='Process some integers')
parser.add_argument('--time', type=int)
args = parser.parse_args()

def get_location():
    """
    Returns:
    str: longitude
    str: latitude
    """
    request = requests.get("https://ipvigilante.com")
    data = request.json()
    print(data["data"]["longitude"], data["data"]["latitude"])
    return data["data"]["longitude"], data["data"]["latitude"]

    # get latitude and longitude for current location (determined by IP)
def get_forecast(longitude, latitude):

    # make the request to the api / returns a response object
    request = requests.get(
        "https://api.darksky.net/forecast/fbcf15d3d872f2f66720690d60e26343/" + latitude + "," + longitude)

    if args.time:
        url = url + str(args.time)

    # get the response data as json
    weather_data_as_json = request.json()

    # get the temp and feelsLike out of the currently dictionary
    temp = weather_data_as_json["currently"]["temperature"]
    feelsLike = weather_data_as_json["currently"]["apparentTemperature"]

    # get high, low and humidity out of the daily dictionary
    high = weather_data_as_json["daily"]["data"][0]["temperatureHigh"]
    low = weather_data_as_json["daily"]["data"][0]["temperatureLow"]
    humidity = weather_data_as_json["daily"]["data"][0]["humidity"]

    # convert values to string
    temp_as_string = str(temp)
    feelsLike_as_string = str(feelsLike)
    high_as_string = str(high)
    low_as_string = str(low)
    humidity_as_string = str(humidity)

    # return values
    return temp_as_string, feelsLike_as_string, high_as_string, low_as_string, humidity_as_string

    # define print forecast variable
def print_forecast(temp_as_string, feelsLike_as_string, high_as_string, low_as_string, humidity_as_string):

    print("Today's forecast is:")
    print("Temperature = " + temp_as_string)
    print("Feels Like = " + feelsLike_as_string)
    print("High = " + high_as_string)
    print("Low = " + low_as_string)
    print("Humidity = " + humidity_as_string)

if __name__ == "__main__":
    longitude, latitude = get_location()
    temp_as_string, feelsLike_as_string, high_as_string, low_as_string, \
        humidity_as_string = get_forecast(longitude, latitude)
    print_forecast(temp_as_string, feelsLike_as_string, high_as_string, low_as_string, humidity_as_string)
